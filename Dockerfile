FROM phoenix-dev:latest

# Fetch the app repository
RUN git clone https://gitlab.com/papylhomme/shoppinglists.git
WORKDIR shoppinglists 

# Fetch dependencies before building JS app (dependency on Phoenix JS)
RUN mix deps.get

# Build JS app
RUN cd assets && \
  npm config set loglevel verbose && \
  npm install --progress && \
  node_modules/webpack/bin/webpack.js --mode=production --progress

# Set a dummy DATABASE_URL (currently required by config/prod.secret.exs)
ENV DATABASE_URL=ecto://user:password@host/db

# Build a release
# - first generate a secret in dev env (phx.gen.secret in prod env fails if SECRET_KEY_BASE is not present...)
# - then produce a release in prod env
RUN export MIX_ENV=dev && \
  export SECRET_KEY_BASE=`mix phx.gen.secret` && \
  export MIX_ENV=prod && \
  mix deps.compile && \
  mix phx.digest && \
  mix compile && \
  mix release

# Start the application
CMD ["_build/prod/rel/shopping_lists/bin/shopping_lists", "start"]
